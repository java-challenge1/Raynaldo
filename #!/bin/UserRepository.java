import java.util.List;

public interface UserRepository {
    void save(User user);
    void update(User user);
    void delete(Long id);
    User findById(Long id);
    User findByUsername(String username);
    List<User> findAll();
}