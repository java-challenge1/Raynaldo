import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    private UserService userService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        userService = new UserService(userRepository);
    }

    @Test
    public void testRegisterUser_SuccessfulRegistration() {
        // Arrange
        String username = "john";
        String password = "password";
        UserRegistrationRequest request = new UserRegistrationRequest(username, password);
        when(userRepository.findByUsername(username)).thenReturn(null);

        // Act
        userService.registerUser(request.getUsername(), request.getPassword());

        // Assert
        verify(userRepository, times(1)).findByUsername(username);
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void testRegisterUser_UsernameAlreadyExists() {
        // Arrange
        String username = "john";
        String password = "password";
        UserRegistrationRequest request = new UserRegistrationRequest(username, password);
        when(userRepository.findByUsername(username)).thenReturn(new User());

        // Act & Assert
        assertThrows(UserRegistrationException.class, () -> {
            userService.registerUser(request.getUsername(), request.getPassword());
        });
        verify(userRepository, times(1)).findByUsername(username);
        verify(userRepository, never()).save(any(User.class));
    }

    @Test
    public void testLoginUser_SuccessfulLogin() {
        // Arrange
        String username = "john";
        String password = "password";
        UserLoginRequest request = new UserLoginRequest(username, password);
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        when(userRepository.findByUsername(username)).thenReturn(user);

        // Act
        userService.loginUser(request.getUsername(), request.getPassword());

        // Assert
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testLoginUser_InvalidCredentials() {
        // Arrange
        String username = "john";
        String password = "password";
        UserLoginRequest request = new UserLoginRequest(username, password);
        when(userRepository.findByUsername(username)).thenReturn(null);

        // Act & Assert
        assertThrows(InvalidCredentialsException.class, () -> {
            userService.loginUser(request.getUsername(), request.getPassword());
        });
        verify(userRepository, times(1)).findByUsername(username);
    }

    // Add more test cases for other methods in UserService

    private static class UserRegistrationRequest {
        private String username;
        private String password;

        public UserRegistrationRequest(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }

    private static class UserLoginRequest {
        private String username;
        private String password;

        public UserLoginRequest(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }
}